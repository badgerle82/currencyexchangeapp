package com.example.currencyexchangeapp.core.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.currencyexchangeapp.core.data.db.AppDatabase.Companion.DB_VERSION
import com.example.currencyexchangeapp.core.data.db.dao.MetaDataDao
import com.example.currencyexchangeapp.core.data.db.dao.RateDao
import com.example.currencyexchangeapp.core.data.db.entity.MetaDataEntity
import com.example.currencyexchangeapp.core.data.db.entity.RateEntity

@Database(
    version = DB_VERSION,
    entities = [
        RateEntity::class,
        MetaDataEntity::class
    ],
    exportSchema = false
)
abstract class AppDatabase : RoomDatabase() {

    companion object {
        const val DB_NAME = "currency_exchange_database"
        const val DB_VERSION = 1
    }

    abstract val rateDao: RateDao

    abstract val metaDataDao: MetaDataDao
}