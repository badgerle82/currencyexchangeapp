package com.example.currencyexchangeapp.core.base

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.example.currencyexchangeapp.core.application.CEApplication
import javax.inject.Inject

abstract class BaseFragment: Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    override fun onAttach(context: Context) {
        super.onAttach(context)
        CEApplication.appComponent.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = getBinding(inflater, container)
        binding.setLifecycleOwner(viewLifecycleOwner)
        return binding.root
    }

    protected abstract fun getBinding(inflater: LayoutInflater, container: ViewGroup?): ViewDataBinding

    abstract fun observeViewModel()

    protected inline fun <reified T : ViewModel> getViewModel(): T =
        ViewModelProviders.of(activity!!, viewModelFactory).get(T::class.java)

    protected inline fun <reified T : ViewModel> getViewModel(scope: Fragment): T =
        ViewModelProviders.of(scope, viewModelFactory).get(T::class.java)
}