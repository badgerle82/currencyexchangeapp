package com.example.currencyexchangeapp.core.model.retrofit

import com.google.gson.annotations.SerializedName
import java.util.*

data class WeekHistoryResponse(
    @SerializedName("base") val base: String,
    @SerializedName("start_at") val startDate: String,
    @SerializedName("end_at") val endDate: String,
    @SerializedName("rates") val rates: SortedMap<String, Map<String, Float>>//TODO check for more elegance solution
)