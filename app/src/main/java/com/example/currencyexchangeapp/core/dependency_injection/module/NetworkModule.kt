package com.example.currencyexchangeapp.core.dependency_injection.module

import com.example.currencyexchangeapp.core.data.network.retrofit.CurrencyExchangeApi
import com.example.currencyexchangeapp.core.dependency_injection.AppScope
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import okhttp3.Authenticator
import okhttp3.CertificatePinner
import okhttp3.ConnectionSpec
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

@Module
class NetworkModule {

    companion object {
        const val TIMEOUT = 60_000L
        const val BASE_URL = "https://api.exchangeratesapi.io/"

    }

    @Provides
    @AppScope
    fun provideMainRetrofit(
         okHttpClient: OkHttpClient,
        gson: Gson
    ): Retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .client(okHttpClient)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build()

    @Provides
    @AppScope
    fun provideMainOkHttpClient(
        loggingInterceptor: HttpLoggingInterceptor
    ): OkHttpClient =
        buildOkHttp(
            loggingInterceptor
        ).build()

    private fun buildOkHttp(
        loggingInterceptor: HttpLoggingInterceptor
    ): OkHttpClient.Builder {
        val okhttpClient = OkHttpClient.Builder()
            .addNetworkInterceptor(loggingInterceptor)
            .readTimeout(TIMEOUT, TimeUnit.MILLISECONDS)
            .writeTimeout(TIMEOUT, TimeUnit.MILLISECONDS)
            .connectTimeout(TIMEOUT, TimeUnit.MILLISECONDS)
        return okhttpClient
    }

    @Provides
    @AppScope
    fun provideHttpLoggingInterceptor(): HttpLoggingInterceptor {
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        return logging
    }

    @Provides
    @AppScope
    fun provideCurrencyExchangeApi(retrofit: Retrofit): CurrencyExchangeApi = retrofit.create(CurrencyExchangeApi::class.java)
}