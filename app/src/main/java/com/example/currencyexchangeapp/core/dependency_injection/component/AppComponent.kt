package com.example.currencyexchangeapp.core.dependency_injection.component

import com.example.currencyexchangeapp.core.base.BaseFragment
import com.example.currencyexchangeapp.core.dependency_injection.AppScope
import com.example.currencyexchangeapp.core.dependency_injection.module.ApplicationModule
import com.example.currencyexchangeapp.core.dependency_injection.module.DataModule
import com.example.currencyexchangeapp.core.dependency_injection.module.NetworkModule
import com.example.currencyexchangeapp.core.dependency_injection.module.ViewModelModule
import dagger.Component

@AppScope
@Component(modules = [
    ApplicationModule::class,
    ViewModelModule::class,
    NetworkModule::class,
    DataModule::class

])
interface AppComponent {

    fun inject(fragment: BaseFragment)
}