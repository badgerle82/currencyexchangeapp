package com.example.currencyexchangeapp.core.data.repository.home

import com.example.currencyexchangeapp.core.data.db.AppDatabase
import com.example.currencyexchangeapp.core.data.db.entity.MetaDataEntity
import com.example.currencyexchangeapp.core.data.db.entity.RateEntity
import com.example.currencyexchangeapp.core.data.network.retrofit.CurrencyExchangeApi
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList

class AppHomeRepository @Inject constructor(
    private val api: CurrencyExchangeApi,
    private val database: AppDatabase
): HomeRepository {

    companion object {
        const val PERIOD_10_MIN = 600000
    }

    override suspend fun getRateEntity(): List<RateEntity> {
        val metaData = database.metaDataDao.getMetadata()
        var lastTimestamp: Long? = null
        if (metaData != null) {
            lastTimestamp = database.metaDataDao.getMetadata()?.lastTimestamp
        }

        val currentTime: Long = Calendar.getInstance().timeInMillis

        if (lastTimestamp != null && lastTimestamp != 0L && (currentTime - lastTimestamp) < PERIOD_10_MIN) {
            return database.rateDao.getRates()
        } else {
            val getLatestCurrencyRates = api.getLatestCurrencyRates()
            if (getLatestCurrencyRates.isSuccessful) {
                database.metaDataDao.insertMetadata(MetaDataEntity(lastTimestamp = currentTime))
                database.rateDao.clear()
                database.rateDao.insertRates(mapRates(getLatestCurrencyRates.body()?.rates))
                return database.rateDao.getRates()
            }
        }
        return emptyList()
    }

    private fun mapRates(map: Map <String, Float>?): List<RateEntity> {
        val rateList = ArrayList<RateEntity>()
        if (!map.isNullOrEmpty()) {
            map.forEach {
                val twoDecimal = (Math.round(it.value * 100.0) / 100.0).toFloat()
                rateList.add(RateEntity(key = it.key, value = twoDecimal))
            }
        }
        return rateList
    }

}