package com.example.currencyexchangeapp.core.application

import android.app.Application
import com.example.currencyexchangeapp.core.dependency_injection.component.AppComponent
import com.example.currencyexchangeapp.core.dependency_injection.component.DaggerAppComponent
import com.example.currencyexchangeapp.core.dependency_injection.module.ApplicationModule

class CEApplication: Application() {
    companion object {
        @JvmStatic
        lateinit var appComponent: AppComponent
            private set
    }

    override fun onCreate() {
        super.onCreate()
        initDaggerAppComponent()
    }

    private fun initDaggerAppComponent() {
        appComponent = DaggerAppComponent
            .builder()
            .applicationModule(
                ApplicationModule(
                    this
                )
            )
            .build()
    }
}