package com.example.currencyexchangeapp.core.data.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.currencyexchangeapp.core.data.db.entity.MetaDataEntity

@Dao
interface MetaDataDao {

    @Query("SELECT * FROM metadata WHERE id = 1")
    suspend fun getMetadata(): MetaDataEntity?

    @Query("DELETE FROM metadata")
    suspend fun clear()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertMetadata(metadata: MetaDataEntity)
}