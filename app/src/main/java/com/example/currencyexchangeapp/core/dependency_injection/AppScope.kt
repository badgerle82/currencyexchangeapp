package com.example.currencyexchangeapp.core.dependency_injection

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class AppScope