package com.example.currencyexchangeapp.core.data.repository.home

import androidx.lifecycle.LiveData
import com.example.currencyexchangeapp.core.data.db.entity.RateEntity

interface HomeRepository {

    suspend fun getRateEntity(): List<RateEntity>
}