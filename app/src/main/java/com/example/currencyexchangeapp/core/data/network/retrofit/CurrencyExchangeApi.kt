package com.example.currencyexchangeapp.core.data.network.retrofit

import androidx.annotation.Keep
import com.example.currencyexchangeapp.core.model.retrofit.LatestCurrencyRatesResponse
import com.example.currencyexchangeapp.core.model.retrofit.WeekHistoryResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

@Keep
interface CurrencyExchangeApi {

    @GET("latest")
    suspend fun getLatestCurrencyRates(@Query("base") base: String = "USD"): Response<LatestCurrencyRatesResponse>

    @GET("history")
    suspend fun getWeekHistory(
        @Query("start_at") start_at: String,
        @Query("end_at") end_at: String,
        @Query("symbols") symbols: String,
        @Query("base") base: String = "USD"

    ): Response<WeekHistoryResponse>
}