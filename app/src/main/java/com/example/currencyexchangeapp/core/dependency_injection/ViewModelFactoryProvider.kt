package com.example.currencyexchangeapp.core.dependency_injection

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import javax.inject.Inject
import javax.inject.Provider

class ViewModelFactoryProvider @Inject constructor(
    private val factories: MutableMap<Class<out ViewModel>, Provider<ViewModelProvider.Factory>>
) {

    fun <T : ViewModel, F : ViewModelProvider.Factory> getViewModelFromFactory(
        vmClass: Class<T>,
        scope: Fragment,
        init: F.() -> Unit = {}
    ) = ViewModelProviders.of(scope, getFactory(vmClass, init)).get(vmClass)

    fun <T : ViewModel, F : ViewModelProvider.Factory> getViewModelFromFactory(
        vmClass: Class<T>,
        scope: FragmentActivity,
        init: F.() -> Unit = {}
    ) = ViewModelProviders.of(scope, getFactory(vmClass, init)).get(vmClass)

    @Suppress("UNCHECKED_CAST")
    private fun <T : ViewModel, F : ViewModelProvider.Factory> getFactory(vmClass: Class<T>, init: F.() -> Unit) =
        ((factories[vmClass]?.get() as? F)
            ?: throw IllegalArgumentException("factory class for $vmClass not found"))
            .apply(init)
}