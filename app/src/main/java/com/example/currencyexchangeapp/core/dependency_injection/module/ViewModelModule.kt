package com.example.currencyexchangeapp.core.dependency_injection.module

import androidx.lifecycle.ViewModelProvider
import com.example.currencyexchangeapp.core.base.BaseViewModel
import com.example.currencyexchangeapp.core.dependency_injection.AppScope
import com.example.currencyexchangeapp.core.dependency_injection.ViewModelFactory
import com.example.currencyexchangeapp.core.dependency_injection.ViewModelKey
import com.example.currencyexchangeapp.features.history.HistoryViewModel
import com.example.currencyexchangeapp.features.home.HomeViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    @AppScope
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(HomeViewModel::class)
    internal abstract fun createHomeViewModel(viewModel: HomeViewModel): BaseViewModel

    @Binds
    @IntoMap
    @ViewModelKey(HistoryViewModel::class)
    internal abstract fun createHistoryViewModel(viewModel: HistoryViewModel): BaseViewModel
}