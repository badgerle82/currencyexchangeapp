package com.example.currencyexchangeapp.core.model.retrofit

import com.google.gson.annotations.SerializedName

data class LatestCurrencyRatesResponse(
    @SerializedName("base") val base: String,
    @SerializedName("date") val date: String,
    @SerializedName("rates") val rates: Map<String, Float>
)