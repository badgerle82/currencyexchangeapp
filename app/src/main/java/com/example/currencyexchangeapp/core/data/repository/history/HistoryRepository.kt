package com.example.currencyexchangeapp.core.data.repository.history

import com.anychart.chart.common.dataentry.ValueDataEntry
import com.example.currencyexchangeapp.core.model.dto.ChartEntity

interface HistoryRepository {
    suspend fun getWeekHistory(currency: String): List<ValueDataEntry>
}