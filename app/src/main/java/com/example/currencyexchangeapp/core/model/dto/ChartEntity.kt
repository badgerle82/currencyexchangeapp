package com.example.currencyexchangeapp.core.model.dto

data class ChartEntity(
    val date: String,
    val title: String,
    val value: Float
)