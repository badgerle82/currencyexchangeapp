package com.example.currencyexchangeapp.core.base

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

abstract class BaseViewModel : ViewModel() {

    val onProgressLiveData = MutableLiveData<Boolean>()

    val hideKeyBoard = MutableLiveData<Boolean>()
}