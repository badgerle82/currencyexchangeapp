package com.example.currencyexchangeapp.core.data.repository.history

import com.anychart.chart.common.dataentry.ValueDataEntry
import com.example.currencyexchangeapp.core.data.db.AppDatabase
import com.example.currencyexchangeapp.core.data.network.retrofit.CurrencyExchangeApi
import com.example.currencyexchangeapp.core.model.dto.ChartEntity
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class AppHistoryRepository @Inject constructor(
    private val database: AppDatabase,
    private val api: CurrencyExchangeApi
): HistoryRepository {

    override suspend fun getWeekHistory(currency: String): List<ValueDataEntry> {
        val dateFormat = SimpleDateFormat("yyyy-MM-dd")
        val currentCal = Calendar.getInstance()
        val toDate = dateFormat.format(currentCal.time)
        currentCal.add(Calendar.DATE, -7)
        val fromDate = dateFormat.format(currentCal.time)

        val getHistoryResponse = api.getWeekHistory(fromDate, toDate, currency)
        return mapHistoryRates(getHistoryResponse.body()?.rates)
    }

    private fun mapHistoryRates(map: Map <String, Map<String, Float>>?): List<ValueDataEntry> {


        val historyRateList = ArrayList<ValueDataEntry>()
        if (!map.isNullOrEmpty()) {
            map.forEach {
                val mappedValue = it.value.entries.first()
//                val floatTo2 = String.format("%.2f", mappedValue.value)
                historyRateList.add(ValueDataEntry(it.key, Math.round(mappedValue.value * 100.0) / 100.0))
            }
        }
        return historyRateList
    }
}