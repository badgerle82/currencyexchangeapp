package com.example.currencyexchangeapp.core.data.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(
    tableName = "metadata"
)
data class MetaDataEntity(
    @PrimaryKey(autoGenerate = false) @ColumnInfo(name = "id") val id: Int = 1,
    @ColumnInfo(name = "base") val base: String = "USD",
    @ColumnInfo(name = "lastTimestamp") val lastTimestamp: Long
)