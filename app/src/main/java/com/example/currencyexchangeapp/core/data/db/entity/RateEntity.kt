package com.example.currencyexchangeapp.core.data.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(
    tableName = "rates"
)
data class RateEntity(
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "id") val id: Int = 0,
    @ColumnInfo(name = "key") val key: String,
    @ColumnInfo(name = "value") val value: Float
)
