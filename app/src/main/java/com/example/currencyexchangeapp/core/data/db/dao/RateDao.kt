package com.example.currencyexchangeapp.core.data.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.currencyexchangeapp.core.data.db.entity.RateEntity

@Dao
interface RateDao {

    @Query("SELECT * FROM rates")
    suspend fun getRates(): List<RateEntity>

    @Query("DELETE FROM rates")
    suspend fun clear()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertRates(rates: List<RateEntity>)
}