package com.example.currencyexchangeapp.core.dependency_injection.module

import android.content.Context
import androidx.room.Room
import com.example.currencyexchangeapp.core.data.db.AppDatabase
import com.example.currencyexchangeapp.core.data.repository.history.AppHistoryRepository
import com.example.currencyexchangeapp.core.data.repository.history.HistoryRepository
import com.example.currencyexchangeapp.core.data.repository.home.AppHomeRepository
import com.example.currencyexchangeapp.core.data.repository.home.HomeRepository
import com.example.currencyexchangeapp.core.dependency_injection.AppScope
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides

@Module
class DataModule {

    @Provides
    @AppScope
    fun provideDatabase(context: Context) =
        Room.databaseBuilder(context, AppDatabase::class.java, AppDatabase.DB_NAME)
            .fallbackToDestructiveMigration()
            .build()

    @Provides
    @AppScope
    fun provideGson(): Gson = GsonBuilder()
            .setLenient()
            .create()

    @Provides
    @AppScope
    fun provideHomeRepository(repositoryImpl: AppHomeRepository): HomeRepository = repositoryImpl

    @Provides
    @AppScope
    fun provideHistoryRepository(repositoryImpl: AppHistoryRepository): HistoryRepository = repositoryImpl
}