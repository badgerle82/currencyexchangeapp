package com.example.currencyexchangeapp.core.dependency_injection.module

import android.content.Context
import com.example.currencyexchangeapp.core.application.CEApplication
import com.example.currencyexchangeapp.core.dependency_injection.AppScope
import dagger.Module
import dagger.Provides

@Module
class ApplicationModule(private val application: CEApplication) {

    @Provides
    @AppScope
    fun provideApplicationContext(): Context = application
}