package com.example.currencyexchangeapp.features.history

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.anychart.chart.common.dataentry.ValueDataEntry
import com.example.currencyexchangeapp.core.base.BaseViewModel
import com.example.currencyexchangeapp.core.data.db.entity.RateEntity
import com.example.currencyexchangeapp.core.data.repository.history.HistoryRepository
import com.example.currencyexchangeapp.core.model.dto.ChartEntity
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import javax.inject.Inject

class HistoryViewModel @Inject constructor(val repository: HistoryRepository): BaseViewModel() {
    val historyLiveData = MutableLiveData<List<ValueDataEntry>>()

    fun getWeekHistory(currency: String): Job =  viewModelScope.launch {
        historyLiveData.postValue(repository.getWeekHistory(currency))
    }

}