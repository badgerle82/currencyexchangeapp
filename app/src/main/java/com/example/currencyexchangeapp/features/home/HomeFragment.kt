package com.example.currencyexchangeapp.features.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.observe
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.currencyexchangeapp.R
import com.example.currencyexchangeapp.core.base.BaseFragment
import com.example.currencyexchangeapp.databinding.HomeFragmentBinding

class HomeFragment: BaseFragment() {

    private lateinit var binding: HomeFragmentBinding
    private lateinit var viewModel: HomeViewModel
    private lateinit var currencyAdapter: CurrencyRecyclerViewAdapter


    override fun getBinding(inflater: LayoutInflater, container: ViewGroup?): ViewDataBinding {
        binding = DataBindingUtil.inflate(inflater, R.layout.home_fragment, container, false)
        return  binding
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = super.onCreateView(inflater, container, savedInstanceState)

        viewModel = getViewModel(this)

        return root
    }

    fun onCurrencyClick(history: String){
        openHistoryFragment(view!!, history)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeViewModel()
        currencyAdapter = CurrencyRecyclerViewAdapter(::onCurrencyClick, emptyList())
        binding.recyclerView.adapter = currencyAdapter
        binding.recyclerView.layoutManager = LinearLayoutManager(this.context, LinearLayoutManager.VERTICAL, false)
        viewModel.getRateEntity()
    }

    override fun observeViewModel() {
        viewModel.ratesLiveData.observe(viewLifecycleOwner) {
            currencyAdapter.setData(it)
        }
    }

    private fun openHistoryFragment(view: View, history: String) {
        val history = history
        val actionHistoryFragment =
            HomeFragmentDirections.actionHistoryFragment(history)
        Navigation.findNavController(view).navigate(actionHistoryFragment)
    }
}