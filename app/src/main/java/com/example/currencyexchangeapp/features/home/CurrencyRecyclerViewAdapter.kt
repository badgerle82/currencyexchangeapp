package com.example.currencyexchangeapp.features.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.currencyexchangeapp.R
import com.example.currencyexchangeapp.core.data.db.entity.RateEntity
import com.example.currencyexchangeapp.databinding.ItemViewExchangeRatesBinding

class CurrencyRecyclerViewAdapter(
    private val itemListener: (String) -> Unit,
    private var currencyList: List<RateEntity>

) : RecyclerView.Adapter<CurrencyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CurrencyViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return CurrencyViewHolder(
            itemListener,
            DataBindingUtil.inflate(layoutInflater, R.layout.item_view_exchange_rates, parent, false))
    }

    override fun getItemCount(): Int = currencyList.size

    override fun onBindViewHolder(holder: CurrencyViewHolder, position: Int) {
        holder.bind(currencyList[position])
    }

    fun setData(list: List<RateEntity>) {
        this.currencyList = list
        notifyDataSetChanged()
    }
}

class CurrencyViewHolder(
    private val itemListener: (String) -> Unit,
    private val binding: ItemViewExchangeRatesBinding
) : RecyclerView.ViewHolder(binding.root) {

    fun bind(model: RateEntity) {
        binding.holder = this
        binding.item = model
    }

    fun onItemClick(rateEntity: RateEntity) {
        itemListener.invoke(rateEntity.key)
    }
}