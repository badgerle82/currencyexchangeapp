package com.example.currencyexchangeapp.features.history

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.anychart.AnyChart
import com.anychart.chart.common.dataentry.DataEntry
import com.example.currencyexchangeapp.R
import com.example.currencyexchangeapp.core.base.BaseFragment
import com.example.currencyexchangeapp.core.extentions.observeNonNull
import com.example.currencyexchangeapp.databinding.HistoryFragmentBinding


class HistoryFragment: BaseFragment() {

    private lateinit var binding: HistoryFragmentBinding
    private lateinit var viewModel: HistoryViewModel
    private val data: MutableList<DataEntry> = ArrayList()

    override fun getBinding(inflater: LayoutInflater, container: ViewGroup?): ViewDataBinding {
        binding = DataBindingUtil.inflate(inflater, R.layout.history_fragment, container, false)
        return  binding
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = super.onCreateView(inflater, container, savedInstanceState)
        viewModel = getViewModel(this)
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeViewModel()
        val bundle = arguments?.let { HistoryFragmentArgs.fromBundle(it) }
        val currency = bundle!!.history
        viewModel.getWeekHistory(currency)
        binding.currTv.text = currency
    }

    override fun observeViewModel() {
        viewModel.historyLiveData.observeNonNull(viewLifecycleOwner){
            if (it.isNotEmpty()) {
                data.addAll(it)
                val chart = AnyChart.line()
                chart.data(data)
                binding.anyChartView.setChart(chart)
            } else {
                Toast.makeText(this.context,"No exchange rate data is available for the selected currency.",Toast.LENGTH_LONG).show()
            }
        }
    }
}