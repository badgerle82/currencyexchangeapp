package com.example.currencyexchangeapp.features.home

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.currencyexchangeapp.core.base.BaseViewModel
import com.example.currencyexchangeapp.core.data.db.entity.RateEntity
import com.example.currencyexchangeapp.core.data.repository.home.HomeRepository
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import javax.inject.Inject

class HomeViewModel @Inject constructor(val repository: HomeRepository): BaseViewModel() {
    val ratesLiveData = MutableLiveData<List<RateEntity>>()

    fun getRateEntity() : Job =  viewModelScope.launch {
        ratesLiveData.postValue(repository.getRateEntity())
        }
}