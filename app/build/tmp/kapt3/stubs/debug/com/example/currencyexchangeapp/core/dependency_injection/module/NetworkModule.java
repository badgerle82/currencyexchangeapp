package com.example.currencyexchangeapp.core.dependency_injection.module;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0007\u0018\u0000 \u00122\u00020\u0001:\u0001\u0012B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0002J\u0010\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nH\u0007J\b\u0010\u000b\u001a\u00020\u0006H\u0007J\u0010\u0010\f\u001a\u00020\r2\u0006\u0010\u0005\u001a\u00020\u0006H\u0007J\u0018\u0010\u000e\u001a\u00020\n2\u0006\u0010\u000f\u001a\u00020\r2\u0006\u0010\u0010\u001a\u00020\u0011H\u0007\u00a8\u0006\u0013"}, d2 = {"Lcom/example/currencyexchangeapp/core/dependency_injection/module/NetworkModule;", "", "()V", "buildOkHttp", "Lokhttp3/OkHttpClient$Builder;", "loggingInterceptor", "Lokhttp3/logging/HttpLoggingInterceptor;", "provideCurrencyExchangeApi", "Lcom/example/currencyexchangeapp/core/data/network/retrofit/CurrencyExchangeApi;", "retrofit", "Lretrofit2/Retrofit;", "provideHttpLoggingInterceptor", "provideMainOkHttpClient", "Lokhttp3/OkHttpClient;", "provideMainRetrofit", "okHttpClient", "gson", "Lcom/google/gson/Gson;", "Companion", "app_debug"})
@dagger.Module()
public final class NetworkModule {
    public static final long TIMEOUT = 60000L;
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String BASE_URL = "https://api.exchangeratesapi.io/";
    public static final com.example.currencyexchangeapp.core.dependency_injection.module.NetworkModule.Companion Companion = null;
    
    @org.jetbrains.annotations.NotNull()
    @com.example.currencyexchangeapp.core.dependency_injection.AppScope()
    @dagger.Provides()
    public final retrofit2.Retrofit provideMainRetrofit(@org.jetbrains.annotations.NotNull()
    okhttp3.OkHttpClient okHttpClient, @org.jetbrains.annotations.NotNull()
    com.google.gson.Gson gson) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @com.example.currencyexchangeapp.core.dependency_injection.AppScope()
    @dagger.Provides()
    public final okhttp3.OkHttpClient provideMainOkHttpClient(@org.jetbrains.annotations.NotNull()
    okhttp3.logging.HttpLoggingInterceptor loggingInterceptor) {
        return null;
    }
    
    private final okhttp3.OkHttpClient.Builder buildOkHttp(okhttp3.logging.HttpLoggingInterceptor loggingInterceptor) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @com.example.currencyexchangeapp.core.dependency_injection.AppScope()
    @dagger.Provides()
    public final okhttp3.logging.HttpLoggingInterceptor provideHttpLoggingInterceptor() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @com.example.currencyexchangeapp.core.dependency_injection.AppScope()
    @dagger.Provides()
    public final com.example.currencyexchangeapp.core.data.network.retrofit.CurrencyExchangeApi provideCurrencyExchangeApi(@org.jetbrains.annotations.NotNull()
    retrofit2.Retrofit retrofit) {
        return null;
    }
    
    public NetworkModule() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\t\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0007"}, d2 = {"Lcom/example/currencyexchangeapp/core/dependency_injection/module/NetworkModule$Companion;", "", "()V", "BASE_URL", "", "TIMEOUT", "", "app_debug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}