package com.example.currencyexchangeapp.core.application;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0018\u0000 \u00062\u00020\u0001:\u0001\u0006B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0004H\u0002J\b\u0010\u0005\u001a\u00020\u0004H\u0016\u00a8\u0006\u0007"}, d2 = {"Lcom/example/currencyexchangeapp/core/application/CEApplication;", "Landroid/app/Application;", "()V", "initDaggerAppComponent", "", "onCreate", "Companion", "app_debug"})
public final class CEApplication extends android.app.Application {
    @org.jetbrains.annotations.NotNull()
    private static com.example.currencyexchangeapp.core.dependency_injection.component.AppComponent appComponent;
    public static final com.example.currencyexchangeapp.core.application.CEApplication.Companion Companion = null;
    
    @java.lang.Override()
    public void onCreate() {
    }
    
    private final void initDaggerAppComponent() {
    }
    
    public CEApplication() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final com.example.currencyexchangeapp.core.dependency_injection.component.AppComponent getAppComponent() {
        return null;
    }
    
    private static final void setAppComponent(com.example.currencyexchangeapp.core.dependency_injection.component.AppComponent p0) {
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R,\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u00048\u0006@BX\u0087.\u00a2\u0006\u0014\n\u0000\u0012\u0004\b\u0006\u0010\u0002\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\n\u00a8\u0006\u000b"}, d2 = {"Lcom/example/currencyexchangeapp/core/application/CEApplication$Companion;", "", "()V", "<set-?>", "Lcom/example/currencyexchangeapp/core/dependency_injection/component/AppComponent;", "appComponent", "appComponent$annotations", "getAppComponent", "()Lcom/example/currencyexchangeapp/core/dependency_injection/component/AppComponent;", "setAppComponent", "(Lcom/example/currencyexchangeapp/core/dependency_injection/component/AppComponent;)V", "app_debug"})
    public static final class Companion {
        
        @java.lang.Deprecated()
        public static void appComponent$annotations() {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.example.currencyexchangeapp.core.dependency_injection.component.AppComponent getAppComponent() {
            return null;
        }
        
        private final void setAppComponent(com.example.currencyexchangeapp.core.dependency_injection.component.AppComponent p0) {
        }
        
        private Companion() {
            super();
        }
    }
}