package com.example.currencyexchangeapp.features.history;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u0018\u00002\u00020\u0001B\u000f\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010R\u001d\u0010\u0005\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u00070\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\f\u00a8\u0006\u0011"}, d2 = {"Lcom/example/currencyexchangeapp/features/history/HistoryViewModel;", "Lcom/example/currencyexchangeapp/core/base/BaseViewModel;", "repository", "Lcom/example/currencyexchangeapp/core/data/repository/history/HistoryRepository;", "(Lcom/example/currencyexchangeapp/core/data/repository/history/HistoryRepository;)V", "historyLiveData", "Landroidx/lifecycle/MutableLiveData;", "", "Lcom/anychart/chart/common/dataentry/ValueDataEntry;", "getHistoryLiveData", "()Landroidx/lifecycle/MutableLiveData;", "getRepository", "()Lcom/example/currencyexchangeapp/core/data/repository/history/HistoryRepository;", "getWeekHistory", "Lkotlinx/coroutines/Job;", "currency", "", "app_debug"})
public final class HistoryViewModel extends com.example.currencyexchangeapp.core.base.BaseViewModel {
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.MutableLiveData<java.util.List<com.anychart.chart.common.dataentry.ValueDataEntry>> historyLiveData = null;
    @org.jetbrains.annotations.NotNull()
    private final com.example.currencyexchangeapp.core.data.repository.history.HistoryRepository repository = null;
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.anychart.chart.common.dataentry.ValueDataEntry>> getHistoryLiveData() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.Job getWeekHistory(@org.jetbrains.annotations.NotNull()
    java.lang.String currency) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.example.currencyexchangeapp.core.data.repository.history.HistoryRepository getRepository() {
        return null;
    }
    
    @javax.inject.Inject()
    public HistoryViewModel(@org.jetbrains.annotations.NotNull()
    com.example.currencyexchangeapp.core.data.repository.history.HistoryRepository repository) {
        super();
    }
}