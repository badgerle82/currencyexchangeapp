package com.example.currencyexchangeapp.core.data.db;

import java.lang.System;

@androidx.room.Database(version = 1, entities = {com.example.currencyexchangeapp.core.data.db.entity.RateEntity.class, com.example.currencyexchangeapp.core.data.db.entity.MetaDataEntity.class}, exportSchema = false)
@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\b\'\u0018\u0000 \u000b2\u00020\u0001:\u0001\u000bB\u0005\u00a2\u0006\u0002\u0010\u0002R\u0012\u0010\u0003\u001a\u00020\u0004X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\b\u0005\u0010\u0006R\u0012\u0010\u0007\u001a\u00020\bX\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\b\t\u0010\n\u00a8\u0006\f"}, d2 = {"Lcom/example/currencyexchangeapp/core/data/db/AppDatabase;", "Landroidx/room/RoomDatabase;", "()V", "metaDataDao", "Lcom/example/currencyexchangeapp/core/data/db/dao/MetaDataDao;", "getMetaDataDao", "()Lcom/example/currencyexchangeapp/core/data/db/dao/MetaDataDao;", "rateDao", "Lcom/example/currencyexchangeapp/core/data/db/dao/RateDao;", "getRateDao", "()Lcom/example/currencyexchangeapp/core/data/db/dao/RateDao;", "Companion", "app_debug"})
public abstract class AppDatabase extends androidx.room.RoomDatabase {
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String DB_NAME = "currency_exchange_database";
    public static final int DB_VERSION = 1;
    public static final com.example.currencyexchangeapp.core.data.db.AppDatabase.Companion Companion = null;
    
    @org.jetbrains.annotations.NotNull()
    public abstract com.example.currencyexchangeapp.core.data.db.dao.RateDao getRateDao();
    
    @org.jetbrains.annotations.NotNull()
    public abstract com.example.currencyexchangeapp.core.data.db.dao.MetaDataDao getMetaDataDao();
    
    public AppDatabase() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0007"}, d2 = {"Lcom/example/currencyexchangeapp/core/data/db/AppDatabase$Companion;", "", "()V", "DB_NAME", "", "DB_VERSION", "", "app_debug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}