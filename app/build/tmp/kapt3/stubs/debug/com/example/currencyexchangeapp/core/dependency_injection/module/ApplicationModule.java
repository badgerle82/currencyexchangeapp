package com.example.currencyexchangeapp.core.dependency_injection.module;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0007\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\b\u0010\u0005\u001a\u00020\u0006H\u0007R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0007"}, d2 = {"Lcom/example/currencyexchangeapp/core/dependency_injection/module/ApplicationModule;", "", "application", "Lcom/example/currencyexchangeapp/core/application/CEApplication;", "(Lcom/example/currencyexchangeapp/core/application/CEApplication;)V", "provideApplicationContext", "Landroid/content/Context;", "app_debug"})
@dagger.Module()
public final class ApplicationModule {
    private final com.example.currencyexchangeapp.core.application.CEApplication application = null;
    
    @org.jetbrains.annotations.NotNull()
    @com.example.currencyexchangeapp.core.dependency_injection.AppScope()
    @dagger.Provides()
    public final android.content.Context provideApplicationContext() {
        return null;
    }
    
    public ApplicationModule(@org.jetbrains.annotations.NotNull()
    com.example.currencyexchangeapp.core.application.CEApplication application) {
        super();
    }
}