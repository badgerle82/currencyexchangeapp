package com.example.currencyexchangeapp.core.data.network.retrofit;

import java.lang.System;

@androidx.annotation.Keep()
@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\bg\u0018\u00002\u00020\u0001J!\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\b\b\u0003\u0010\u0005\u001a\u00020\u0006H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0007J?\u0010\b\u001a\b\u0012\u0004\u0012\u00020\t0\u00032\b\b\u0001\u0010\n\u001a\u00020\u00062\b\b\u0001\u0010\u000b\u001a\u00020\u00062\b\b\u0001\u0010\f\u001a\u00020\u00062\b\b\u0003\u0010\u0005\u001a\u00020\u0006H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\r\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\u000e"}, d2 = {"Lcom/example/currencyexchangeapp/core/data/network/retrofit/CurrencyExchangeApi;", "", "getLatestCurrencyRates", "Lretrofit2/Response;", "Lcom/example/currencyexchangeapp/core/model/retrofit/LatestCurrencyRatesResponse;", "base", "", "(Ljava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "getWeekHistory", "Lcom/example/currencyexchangeapp/core/model/retrofit/WeekHistoryResponse;", "start_at", "end_at", "symbols", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "app_debug"})
public abstract interface CurrencyExchangeApi {
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.GET(value = "latest")
    public abstract java.lang.Object getLatestCurrencyRates(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "base")
    java.lang.String base, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.example.currencyexchangeapp.core.model.retrofit.LatestCurrencyRatesResponse>> p1);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.GET(value = "history")
    public abstract java.lang.Object getWeekHistory(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "start_at")
    java.lang.String start_at, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "end_at")
    java.lang.String end_at, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "symbols")
    java.lang.String symbols, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "base")
    java.lang.String base, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.example.currencyexchangeapp.core.model.retrofit.WeekHistoryResponse>> p4);
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 3)
    public final class DefaultImpls {
    }
}