package com.example.currencyexchangeapp.core.data.repository.history;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010$\n\u0002\u0010\u0007\n\u0000\u0018\u00002\u00020\u0001B\u0017\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u001f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\b2\u0006\u0010\n\u001a\u00020\u000bH\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\fJ0\u0010\r\u001a\b\u0012\u0004\u0012\u00020\t0\b2 \u0010\u000e\u001a\u001c\u0012\u0004\u0012\u00020\u000b\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\u00100\u000f\u0018\u00010\u000fH\u0002R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\u0011"}, d2 = {"Lcom/example/currencyexchangeapp/core/data/repository/history/AppHistoryRepository;", "Lcom/example/currencyexchangeapp/core/data/repository/history/HistoryRepository;", "database", "Lcom/example/currencyexchangeapp/core/data/db/AppDatabase;", "api", "Lcom/example/currencyexchangeapp/core/data/network/retrofit/CurrencyExchangeApi;", "(Lcom/example/currencyexchangeapp/core/data/db/AppDatabase;Lcom/example/currencyexchangeapp/core/data/network/retrofit/CurrencyExchangeApi;)V", "getWeekHistory", "", "Lcom/anychart/chart/common/dataentry/ValueDataEntry;", "currency", "", "(Ljava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "mapHistoryRates", "map", "", "", "app_debug"})
public final class AppHistoryRepository implements com.example.currencyexchangeapp.core.data.repository.history.HistoryRepository {
    private final com.example.currencyexchangeapp.core.data.db.AppDatabase database = null;
    private final com.example.currencyexchangeapp.core.data.network.retrofit.CurrencyExchangeApi api = null;
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object getWeekHistory(@org.jetbrains.annotations.NotNull()
    java.lang.String currency, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super java.util.List<? extends com.anychart.chart.common.dataentry.ValueDataEntry>> p1) {
        return null;
    }
    
    private final java.util.List<com.anychart.chart.common.dataentry.ValueDataEntry> mapHistoryRates(java.util.Map<java.lang.String, ? extends java.util.Map<java.lang.String, java.lang.Float>> map) {
        return null;
    }
    
    @javax.inject.Inject()
    public AppHistoryRepository(@org.jetbrains.annotations.NotNull()
    com.example.currencyexchangeapp.core.data.db.AppDatabase database, @org.jetbrains.annotations.NotNull()
    com.example.currencyexchangeapp.core.data.network.retrofit.CurrencyExchangeApi api) {
        super();
    }
}