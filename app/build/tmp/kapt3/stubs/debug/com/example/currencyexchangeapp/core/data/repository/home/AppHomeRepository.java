package com.example.currencyexchangeapp.core.data.repository.home;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\u0010\u0007\n\u0002\b\u0002\u0018\u0000 \u00102\u00020\u0001:\u0001\u0010B\u0017\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0017\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\bH\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\nJ$\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\t0\b2\u0014\u0010\f\u001a\u0010\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u000f\u0018\u00010\rH\u0002R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\u0011"}, d2 = {"Lcom/example/currencyexchangeapp/core/data/repository/home/AppHomeRepository;", "Lcom/example/currencyexchangeapp/core/data/repository/home/HomeRepository;", "api", "Lcom/example/currencyexchangeapp/core/data/network/retrofit/CurrencyExchangeApi;", "database", "Lcom/example/currencyexchangeapp/core/data/db/AppDatabase;", "(Lcom/example/currencyexchangeapp/core/data/network/retrofit/CurrencyExchangeApi;Lcom/example/currencyexchangeapp/core/data/db/AppDatabase;)V", "getRateEntity", "", "Lcom/example/currencyexchangeapp/core/data/db/entity/RateEntity;", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "mapRates", "map", "", "", "", "Companion", "app_debug"})
public final class AppHomeRepository implements com.example.currencyexchangeapp.core.data.repository.home.HomeRepository {
    private final com.example.currencyexchangeapp.core.data.network.retrofit.CurrencyExchangeApi api = null;
    private final com.example.currencyexchangeapp.core.data.db.AppDatabase database = null;
    public static final int PERIOD_10_MIN = 600000;
    public static final com.example.currencyexchangeapp.core.data.repository.home.AppHomeRepository.Companion Companion = null;
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object getRateEntity(@org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super java.util.List<com.example.currencyexchangeapp.core.data.db.entity.RateEntity>> p0) {
        return null;
    }
    
    private final java.util.List<com.example.currencyexchangeapp.core.data.db.entity.RateEntity> mapRates(java.util.Map<java.lang.String, java.lang.Float> map) {
        return null;
    }
    
    @javax.inject.Inject()
    public AppHomeRepository(@org.jetbrains.annotations.NotNull()
    com.example.currencyexchangeapp.core.data.network.retrofit.CurrencyExchangeApi api, @org.jetbrains.annotations.NotNull()
    com.example.currencyexchangeapp.core.data.db.AppDatabase database) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0005"}, d2 = {"Lcom/example/currencyexchangeapp/core/data/repository/home/AppHomeRepository$Companion;", "", "()V", "PERIOD_10_MIN", "", "app_debug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}