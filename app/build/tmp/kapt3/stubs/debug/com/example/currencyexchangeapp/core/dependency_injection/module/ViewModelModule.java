package com.example.currencyexchangeapp.core.dependency_injection.module;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\'\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0015\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H!\u00a2\u0006\u0002\b\u0007J\u0015\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bH!\u00a2\u0006\u0002\b\fJ\u0015\u0010\r\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000eH!\u00a2\u0006\u0002\b\u000f\u00a8\u0006\u0010"}, d2 = {"Lcom/example/currencyexchangeapp/core/dependency_injection/module/ViewModelModule;", "", "()V", "bindViewModelFactory", "Landroidx/lifecycle/ViewModelProvider$Factory;", "factory", "Lcom/example/currencyexchangeapp/core/dependency_injection/ViewModelFactory;", "bindViewModelFactory$app_debug", "createHistoryViewModel", "Lcom/example/currencyexchangeapp/core/base/BaseViewModel;", "viewModel", "Lcom/example/currencyexchangeapp/features/history/HistoryViewModel;", "createHistoryViewModel$app_debug", "createHomeViewModel", "Lcom/example/currencyexchangeapp/features/home/HomeViewModel;", "createHomeViewModel$app_debug", "app_debug"})
@dagger.Module()
public abstract class ViewModelModule {
    
    @org.jetbrains.annotations.NotNull()
    @com.example.currencyexchangeapp.core.dependency_injection.AppScope()
    @dagger.Binds()
    public abstract androidx.lifecycle.ViewModelProvider.Factory bindViewModelFactory$app_debug(@org.jetbrains.annotations.NotNull()
    com.example.currencyexchangeapp.core.dependency_injection.ViewModelFactory factory);
    
    @org.jetbrains.annotations.NotNull()
    @com.example.currencyexchangeapp.core.dependency_injection.ViewModelKey(value = com.example.currencyexchangeapp.features.home.HomeViewModel.class)
    @dagger.multibindings.IntoMap()
    @dagger.Binds()
    public abstract com.example.currencyexchangeapp.core.base.BaseViewModel createHomeViewModel$app_debug(@org.jetbrains.annotations.NotNull()
    com.example.currencyexchangeapp.features.home.HomeViewModel viewModel);
    
    @org.jetbrains.annotations.NotNull()
    @com.example.currencyexchangeapp.core.dependency_injection.ViewModelKey(value = com.example.currencyexchangeapp.features.history.HistoryViewModel.class)
    @dagger.multibindings.IntoMap()
    @dagger.Binds()
    public abstract com.example.currencyexchangeapp.core.base.BaseViewModel createHistoryViewModel$app_debug(@org.jetbrains.annotations.NotNull()
    com.example.currencyexchangeapp.features.history.HistoryViewModel viewModel);
    
    public ViewModelModule() {
        super();
    }
}