package com.example.currencyexchangeapp.core.dependency_injection.module;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b\u0007\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0007J\b\u0010\u0007\u001a\u00020\bH\u0007J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fH\u0007J\u0010\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000b\u001a\u00020\u000fH\u0007\u00a8\u0006\u0010"}, d2 = {"Lcom/example/currencyexchangeapp/core/dependency_injection/module/DataModule;", "", "()V", "provideDatabase", "Lcom/example/currencyexchangeapp/core/data/db/AppDatabase;", "context", "Landroid/content/Context;", "provideGson", "Lcom/google/gson/Gson;", "provideHistoryRepository", "Lcom/example/currencyexchangeapp/core/data/repository/history/HistoryRepository;", "repositoryImpl", "Lcom/example/currencyexchangeapp/core/data/repository/history/AppHistoryRepository;", "provideHomeRepository", "Lcom/example/currencyexchangeapp/core/data/repository/home/HomeRepository;", "Lcom/example/currencyexchangeapp/core/data/repository/home/AppHomeRepository;", "app_debug"})
@dagger.Module()
public final class DataModule {
    
    @org.jetbrains.annotations.NotNull()
    @com.example.currencyexchangeapp.core.dependency_injection.AppScope()
    @dagger.Provides()
    public final com.example.currencyexchangeapp.core.data.db.AppDatabase provideDatabase(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @com.example.currencyexchangeapp.core.dependency_injection.AppScope()
    @dagger.Provides()
    public final com.google.gson.Gson provideGson() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @com.example.currencyexchangeapp.core.dependency_injection.AppScope()
    @dagger.Provides()
    public final com.example.currencyexchangeapp.core.data.repository.home.HomeRepository provideHomeRepository(@org.jetbrains.annotations.NotNull()
    com.example.currencyexchangeapp.core.data.repository.home.AppHomeRepository repositoryImpl) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @com.example.currencyexchangeapp.core.dependency_injection.AppScope()
    @dagger.Provides()
    public final com.example.currencyexchangeapp.core.data.repository.history.HistoryRepository provideHistoryRepository(@org.jetbrains.annotations.NotNull()
    com.example.currencyexchangeapp.core.data.repository.history.AppHistoryRepository repositoryImpl) {
        return null;
    }
    
    public DataModule() {
        super();
    }
}