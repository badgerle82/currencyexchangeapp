package com.example.currencyexchangeapp.core.dependency_injection;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010%\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B)\b\u0007\u0012 \u0010\u0002\u001a\u001c\u0012\f\u0012\n\u0012\u0006\b\u0001\u0012\u00020\u00050\u0004\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00070\u00060\u0003\u00a2\u0006\u0002\u0010\bJH\u0010\t\u001a\u0002H\n\"\b\b\u0000\u0010\u000b*\u00020\u0005\"\b\b\u0001\u0010\n*\u00020\u00072\f\u0010\f\u001a\b\u0012\u0004\u0012\u0002H\u000b0\u00042\u0017\u0010\r\u001a\u0013\u0012\u0004\u0012\u0002H\n\u0012\u0004\u0012\u00020\u000f0\u000e\u00a2\u0006\u0002\b\u0010H\u0002\u00a2\u0006\u0002\u0010\u0011JP\u0010\u0012\u001a\u0002H\u000b\"\b\b\u0000\u0010\u000b*\u00020\u0005\"\b\b\u0001\u0010\n*\u00020\u00072\f\u0010\f\u001a\b\u0012\u0004\u0012\u0002H\u000b0\u00042\u0006\u0010\u0013\u001a\u00020\u00142\u0019\b\u0002\u0010\r\u001a\u0013\u0012\u0004\u0012\u0002H\n\u0012\u0004\u0012\u00020\u000f0\u000e\u00a2\u0006\u0002\b\u0010\u00a2\u0006\u0002\u0010\u0015JP\u0010\u0012\u001a\u0002H\u000b\"\b\b\u0000\u0010\u000b*\u00020\u0005\"\b\b\u0001\u0010\n*\u00020\u00072\f\u0010\f\u001a\b\u0012\u0004\u0012\u0002H\u000b0\u00042\u0006\u0010\u0013\u001a\u00020\u00162\u0019\b\u0002\u0010\r\u001a\u0013\u0012\u0004\u0012\u0002H\n\u0012\u0004\u0012\u00020\u000f0\u000e\u00a2\u0006\u0002\b\u0010\u00a2\u0006\u0002\u0010\u0017R(\u0010\u0002\u001a\u001c\u0012\f\u0012\n\u0012\u0006\b\u0001\u0012\u00020\u00050\u0004\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00070\u00060\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0018"}, d2 = {"Lcom/example/currencyexchangeapp/core/dependency_injection/ViewModelFactoryProvider;", "", "factories", "", "Ljava/lang/Class;", "Landroidx/lifecycle/ViewModel;", "Ljavax/inject/Provider;", "Landroidx/lifecycle/ViewModelProvider$Factory;", "(Ljava/util/Map;)V", "getFactory", "F", "T", "vmClass", "init", "Lkotlin/Function1;", "", "Lkotlin/ExtensionFunctionType;", "(Ljava/lang/Class;Lkotlin/jvm/functions/Function1;)Landroidx/lifecycle/ViewModelProvider$Factory;", "getViewModelFromFactory", "scope", "Landroidx/fragment/app/Fragment;", "(Ljava/lang/Class;Landroidx/fragment/app/Fragment;Lkotlin/jvm/functions/Function1;)Landroidx/lifecycle/ViewModel;", "Landroidx/fragment/app/FragmentActivity;", "(Ljava/lang/Class;Landroidx/fragment/app/FragmentActivity;Lkotlin/jvm/functions/Function1;)Landroidx/lifecycle/ViewModel;", "app_debug"})
public final class ViewModelFactoryProvider {
    private final java.util.Map<java.lang.Class<? extends androidx.lifecycle.ViewModel>, javax.inject.Provider<androidx.lifecycle.ViewModelProvider.Factory>> factories = null;
    
    @org.jetbrains.annotations.NotNull()
    public final <T extends androidx.lifecycle.ViewModel, F extends androidx.lifecycle.ViewModelProvider.Factory>T getViewModelFromFactory(@org.jetbrains.annotations.NotNull()
    java.lang.Class<T> vmClass, @org.jetbrains.annotations.NotNull()
    androidx.fragment.app.Fragment scope, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super F, kotlin.Unit> init) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final <T extends androidx.lifecycle.ViewModel, F extends androidx.lifecycle.ViewModelProvider.Factory>T getViewModelFromFactory(@org.jetbrains.annotations.NotNull()
    java.lang.Class<T> vmClass, @org.jetbrains.annotations.NotNull()
    androidx.fragment.app.FragmentActivity scope, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super F, kotlin.Unit> init) {
        return null;
    }
    
    @kotlin.Suppress(names = {"UNCHECKED_CAST"})
    private final <T extends androidx.lifecycle.ViewModel, F extends androidx.lifecycle.ViewModelProvider.Factory>F getFactory(java.lang.Class<T> vmClass, kotlin.jvm.functions.Function1<? super F, kotlin.Unit> init) {
        return null;
    }
    
    @javax.inject.Inject()
    public ViewModelFactoryProvider(@org.jetbrains.annotations.NotNull()
    java.util.Map<java.lang.Class<? extends androidx.lifecycle.ViewModel>, javax.inject.Provider<androidx.lifecycle.ViewModelProvider.Factory>> factories) {
        super();
    }
}