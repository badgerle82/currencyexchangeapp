package com.example.currencyexchangeapp.databinding;
import com.example.currencyexchangeapp.R;
import com.example.currencyexchangeapp.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ItemViewExchangeRatesBindingImpl extends ItemViewExchangeRatesBinding implements com.example.currencyexchangeapp.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = null;
    }
    // views
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback1;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ItemViewExchangeRatesBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 3, sIncludes, sViewsWithIds));
    }
    private ItemViewExchangeRatesBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[0]
            , (android.widget.TextView) bindings[1]
            , (android.widget.TextView) bindings[2]
            );
        this.containerCurrency.setTag(null);
        this.titleCurrency.setTag(null);
        this.valueCurrency.setTag(null);
        setRootTag(root);
        // listeners
        mCallback1 = new com.example.currencyexchangeapp.generated.callback.OnClickListener(this, 1);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x4L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.item == variableId) {
            setItem((com.example.currencyexchangeapp.core.data.db.entity.RateEntity) variable);
        }
        else if (BR.holder == variableId) {
            setHolder((com.example.currencyexchangeapp.features.home.CurrencyViewHolder) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setItem(@Nullable com.example.currencyexchangeapp.core.data.db.entity.RateEntity Item) {
        this.mItem = Item;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.item);
        super.requestRebind();
    }
    public void setHolder(@Nullable com.example.currencyexchangeapp.features.home.CurrencyViewHolder Holder) {
        this.mHolder = Holder;
        synchronized(this) {
            mDirtyFlags |= 0x2L;
        }
        notifyPropertyChanged(BR.holder);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        float itemValue = 0f;
        java.lang.String itemKey = null;
        com.example.currencyexchangeapp.core.data.db.entity.RateEntity item = mItem;
        java.lang.String stringValueOfItemValue = null;
        com.example.currencyexchangeapp.features.home.CurrencyViewHolder holder = mHolder;

        if ((dirtyFlags & 0x5L) != 0) {



                if (item != null) {
                    // read item.value
                    itemValue = item.getValue();
                    // read item.key
                    itemKey = item.getKey();
                }


                // read String.valueOf(item.value)
                stringValueOfItemValue = java.lang.String.valueOf(itemValue);
        }
        // batch finished
        if ((dirtyFlags & 0x4L) != 0) {
            // api target 1

            this.containerCurrency.setOnClickListener(mCallback1);
        }
        if ((dirtyFlags & 0x5L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.titleCurrency, itemKey);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.valueCurrency, stringValueOfItemValue);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        // localize variables for thread safety
        // item
        com.example.currencyexchangeapp.core.data.db.entity.RateEntity item = mItem;
        // holder
        com.example.currencyexchangeapp.features.home.CurrencyViewHolder holder = mHolder;
        // holder != null
        boolean holderJavaLangObjectNull = false;



        holderJavaLangObjectNull = (holder) != (null);
        if (holderJavaLangObjectNull) {



            holder.onItemClick(item);
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): item
        flag 1 (0x2L): holder
        flag 2 (0x3L): null
    flag mapping end*/
    //end
}