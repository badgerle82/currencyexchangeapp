package com.example.currencyexchangeapp.core.data.db;

import androidx.room.DatabaseConfiguration;
import androidx.room.InvalidationTracker;
import androidx.room.RoomOpenHelper;
import androidx.room.RoomOpenHelper.Delegate;
import androidx.room.RoomOpenHelper.ValidationResult;
import androidx.room.util.DBUtil;
import androidx.room.util.TableInfo;
import androidx.room.util.TableInfo.Column;
import androidx.room.util.TableInfo.ForeignKey;
import androidx.room.util.TableInfo.Index;
import androidx.sqlite.db.SupportSQLiteDatabase;
import androidx.sqlite.db.SupportSQLiteOpenHelper;
import androidx.sqlite.db.SupportSQLiteOpenHelper.Callback;
import androidx.sqlite.db.SupportSQLiteOpenHelper.Configuration;
import com.example.currencyexchangeapp.core.data.db.dao.MetaDataDao;
import com.example.currencyexchangeapp.core.data.db.dao.MetaDataDao_Impl;
import com.example.currencyexchangeapp.core.data.db.dao.RateDao;
import com.example.currencyexchangeapp.core.data.db.dao.RateDao_Impl;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

@SuppressWarnings({"unchecked", "deprecation"})
public final class AppDatabase_Impl extends AppDatabase {
  private volatile RateDao _rateDao;

  private volatile MetaDataDao _metaDataDao;

  @Override
  protected SupportSQLiteOpenHelper createOpenHelper(DatabaseConfiguration configuration) {
    final SupportSQLiteOpenHelper.Callback _openCallback = new RoomOpenHelper(configuration, new RoomOpenHelper.Delegate(1) {
      @Override
      public void createAllTables(SupportSQLiteDatabase _db) {
        _db.execSQL("CREATE TABLE IF NOT EXISTS `rates` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `key` TEXT NOT NULL, `value` REAL NOT NULL)");
        _db.execSQL("CREATE TABLE IF NOT EXISTS `metadata` (`id` INTEGER NOT NULL, `base` TEXT NOT NULL, `lastTimestamp` INTEGER NOT NULL, PRIMARY KEY(`id`))");
        _db.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
        _db.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, '1de1796da0ed7a5bb71ced4287297122')");
      }

      @Override
      public void dropAllTables(SupportSQLiteDatabase _db) {
        _db.execSQL("DROP TABLE IF EXISTS `rates`");
        _db.execSQL("DROP TABLE IF EXISTS `metadata`");
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onDestructiveMigration(_db);
          }
        }
      }

      @Override
      protected void onCreate(SupportSQLiteDatabase _db) {
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onCreate(_db);
          }
        }
      }

      @Override
      public void onOpen(SupportSQLiteDatabase _db) {
        mDatabase = _db;
        internalInitInvalidationTracker(_db);
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onOpen(_db);
          }
        }
      }

      @Override
      public void onPreMigrate(SupportSQLiteDatabase _db) {
        DBUtil.dropFtsSyncTriggers(_db);
      }

      @Override
      public void onPostMigrate(SupportSQLiteDatabase _db) {
      }

      @Override
      protected RoomOpenHelper.ValidationResult onValidateSchema(SupportSQLiteDatabase _db) {
        final HashMap<String, TableInfo.Column> _columnsRates = new HashMap<String, TableInfo.Column>(3);
        _columnsRates.put("id", new TableInfo.Column("id", "INTEGER", true, 1, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsRates.put("key", new TableInfo.Column("key", "TEXT", true, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsRates.put("value", new TableInfo.Column("value", "REAL", true, 0, null, TableInfo.CREATED_FROM_ENTITY));
        final HashSet<TableInfo.ForeignKey> _foreignKeysRates = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesRates = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoRates = new TableInfo("rates", _columnsRates, _foreignKeysRates, _indicesRates);
        final TableInfo _existingRates = TableInfo.read(_db, "rates");
        if (! _infoRates.equals(_existingRates)) {
          return new RoomOpenHelper.ValidationResult(false, "rates(com.example.currencyexchangeapp.core.data.db.entity.RateEntity).\n"
                  + " Expected:\n" + _infoRates + "\n"
                  + " Found:\n" + _existingRates);
        }
        final HashMap<String, TableInfo.Column> _columnsMetadata = new HashMap<String, TableInfo.Column>(3);
        _columnsMetadata.put("id", new TableInfo.Column("id", "INTEGER", true, 1, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsMetadata.put("base", new TableInfo.Column("base", "TEXT", true, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsMetadata.put("lastTimestamp", new TableInfo.Column("lastTimestamp", "INTEGER", true, 0, null, TableInfo.CREATED_FROM_ENTITY));
        final HashSet<TableInfo.ForeignKey> _foreignKeysMetadata = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesMetadata = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoMetadata = new TableInfo("metadata", _columnsMetadata, _foreignKeysMetadata, _indicesMetadata);
        final TableInfo _existingMetadata = TableInfo.read(_db, "metadata");
        if (! _infoMetadata.equals(_existingMetadata)) {
          return new RoomOpenHelper.ValidationResult(false, "metadata(com.example.currencyexchangeapp.core.data.db.entity.MetaDataEntity).\n"
                  + " Expected:\n" + _infoMetadata + "\n"
                  + " Found:\n" + _existingMetadata);
        }
        return new RoomOpenHelper.ValidationResult(true, null);
      }
    }, "1de1796da0ed7a5bb71ced4287297122", "4aee626a458239adb6b89ecd20916a77");
    final SupportSQLiteOpenHelper.Configuration _sqliteConfig = SupportSQLiteOpenHelper.Configuration.builder(configuration.context)
        .name(configuration.name)
        .callback(_openCallback)
        .build();
    final SupportSQLiteOpenHelper _helper = configuration.sqliteOpenHelperFactory.create(_sqliteConfig);
    return _helper;
  }

  @Override
  protected InvalidationTracker createInvalidationTracker() {
    final HashMap<String, String> _shadowTablesMap = new HashMap<String, String>(0);
    HashMap<String, Set<String>> _viewTables = new HashMap<String, Set<String>>(0);
    return new InvalidationTracker(this, _shadowTablesMap, _viewTables, "rates","metadata");
  }

  @Override
  public void clearAllTables() {
    super.assertNotMainThread();
    final SupportSQLiteDatabase _db = super.getOpenHelper().getWritableDatabase();
    try {
      super.beginTransaction();
      _db.execSQL("DELETE FROM `rates`");
      _db.execSQL("DELETE FROM `metadata`");
      super.setTransactionSuccessful();
    } finally {
      super.endTransaction();
      _db.query("PRAGMA wal_checkpoint(FULL)").close();
      if (!_db.inTransaction()) {
        _db.execSQL("VACUUM");
      }
    }
  }

  @Override
  public RateDao getRateDao() {
    if (_rateDao != null) {
      return _rateDao;
    } else {
      synchronized(this) {
        if(_rateDao == null) {
          _rateDao = new RateDao_Impl(this);
        }
        return _rateDao;
      }
    }
  }

  @Override
  public MetaDataDao getMetaDataDao() {
    if (_metaDataDao != null) {
      return _metaDataDao;
    } else {
      synchronized(this) {
        if(_metaDataDao == null) {
          _metaDataDao = new MetaDataDao_Impl(this);
        }
        return _metaDataDao;
      }
    }
  }
}
