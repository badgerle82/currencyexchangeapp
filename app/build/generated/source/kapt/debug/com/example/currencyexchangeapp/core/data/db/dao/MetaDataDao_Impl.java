package com.example.currencyexchangeapp.core.data.db.dao;

import android.database.Cursor;
import androidx.room.CoroutinesRoom;
import androidx.room.EntityInsertionAdapter;
import androidx.room.RoomDatabase;
import androidx.room.RoomSQLiteQuery;
import androidx.room.SharedSQLiteStatement;
import androidx.room.util.CursorUtil;
import androidx.room.util.DBUtil;
import androidx.sqlite.db.SupportSQLiteStatement;
import com.example.currencyexchangeapp.core.data.db.entity.MetaDataEntity;
import java.lang.Exception;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.concurrent.Callable;
import kotlin.Unit;
import kotlin.coroutines.Continuation;

@SuppressWarnings({"unchecked", "deprecation"})
public final class MetaDataDao_Impl implements MetaDataDao {
  private final RoomDatabase __db;

  private final EntityInsertionAdapter<MetaDataEntity> __insertionAdapterOfMetaDataEntity;

  private final SharedSQLiteStatement __preparedStmtOfClear;

  public MetaDataDao_Impl(RoomDatabase __db) {
    this.__db = __db;
    this.__insertionAdapterOfMetaDataEntity = new EntityInsertionAdapter<MetaDataEntity>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR REPLACE INTO `metadata` (`id`,`base`,`lastTimestamp`) VALUES (?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, MetaDataEntity value) {
        stmt.bindLong(1, value.getId());
        if (value.getBase() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.getBase());
        }
        stmt.bindLong(3, value.getLastTimestamp());
      }
    };
    this.__preparedStmtOfClear = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "DELETE FROM metadata";
        return _query;
      }
    };
  }

  @Override
  public Object insertMetadata(final MetaDataEntity metadata, final Continuation<? super Unit> p1) {
    return CoroutinesRoom.execute(__db, true, new Callable<Unit>() {
      @Override
      public Unit call() throws Exception {
        __db.beginTransaction();
        try {
          __insertionAdapterOfMetaDataEntity.insert(metadata);
          __db.setTransactionSuccessful();
          return Unit.INSTANCE;
        } finally {
          __db.endTransaction();
        }
      }
    }, p1);
  }

  @Override
  public Object clear(final Continuation<? super Unit> p0) {
    return CoroutinesRoom.execute(__db, true, new Callable<Unit>() {
      @Override
      public Unit call() throws Exception {
        final SupportSQLiteStatement _stmt = __preparedStmtOfClear.acquire();
        __db.beginTransaction();
        try {
          _stmt.executeUpdateDelete();
          __db.setTransactionSuccessful();
          return Unit.INSTANCE;
        } finally {
          __db.endTransaction();
          __preparedStmtOfClear.release(_stmt);
        }
      }
    }, p0);
  }

  @Override
  public Object getMetadata(final Continuation<? super MetaDataEntity> p0) {
    final String _sql = "SELECT * FROM metadata WHERE id = 1";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    return CoroutinesRoom.execute(__db, false, new Callable<MetaDataEntity>() {
      @Override
      public MetaDataEntity call() throws Exception {
        final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
        try {
          final int _cursorIndexOfId = CursorUtil.getColumnIndexOrThrow(_cursor, "id");
          final int _cursorIndexOfBase = CursorUtil.getColumnIndexOrThrow(_cursor, "base");
          final int _cursorIndexOfLastTimestamp = CursorUtil.getColumnIndexOrThrow(_cursor, "lastTimestamp");
          final MetaDataEntity _result;
          if(_cursor.moveToFirst()) {
            final int _tmpId;
            _tmpId = _cursor.getInt(_cursorIndexOfId);
            final String _tmpBase;
            _tmpBase = _cursor.getString(_cursorIndexOfBase);
            final long _tmpLastTimestamp;
            _tmpLastTimestamp = _cursor.getLong(_cursorIndexOfLastTimestamp);
            _result = new MetaDataEntity(_tmpId,_tmpBase,_tmpLastTimestamp);
          } else {
            _result = null;
          }
          return _result;
        } finally {
          _cursor.close();
          _statement.release();
        }
      }
    }, p0);
  }
}
