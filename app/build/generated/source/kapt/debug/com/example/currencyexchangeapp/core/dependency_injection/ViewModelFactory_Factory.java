// Generated by Dagger (https://google.github.io/dagger).
package com.example.currencyexchangeapp.core.dependency_injection;

import androidx.lifecycle.ViewModel;
import com.example.currencyexchangeapp.core.base.BaseViewModel;
import dagger.internal.Factory;
import java.util.Map;
import javax.inject.Provider;

public final class ViewModelFactory_Factory implements Factory<ViewModelFactory> {
  private final Provider<Map<Class<? extends ViewModel>, Provider<BaseViewModel>>>
      viewModelsProvider;

  public ViewModelFactory_Factory(
      Provider<Map<Class<? extends ViewModel>, Provider<BaseViewModel>>> viewModelsProvider) {
    this.viewModelsProvider = viewModelsProvider;
  }

  @Override
  public ViewModelFactory get() {
    return new ViewModelFactory(viewModelsProvider.get());
  }

  public static ViewModelFactory_Factory create(
      Provider<Map<Class<? extends ViewModel>, Provider<BaseViewModel>>> viewModelsProvider) {
    return new ViewModelFactory_Factory(viewModelsProvider);
  }

  public static ViewModelFactory newViewModelFactory(
      Map<Class<? extends ViewModel>, Provider<BaseViewModel>> viewModels) {
    return new ViewModelFactory(viewModels);
  }
}
