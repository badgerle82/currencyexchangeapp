package com.example.currencyexchangeapp.features.home;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.navigation.NavDirections;
import com.example.currencyexchangeapp.R;
import java.lang.IllegalArgumentException;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.HashMap;

public class HomeFragmentDirections {
  private HomeFragmentDirections() {
  }

  @NonNull
  public static ActionHistoryFragment actionHistoryFragment(@NonNull String history) {
    return new ActionHistoryFragment(history);
  }

  public static class ActionHistoryFragment implements NavDirections {
    private final HashMap arguments = new HashMap();

    private ActionHistoryFragment(@NonNull String history) {
      if (history == null) {
        throw new IllegalArgumentException("Argument \"history\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("history", history);
    }

    @NonNull
    public ActionHistoryFragment setHistory(@NonNull String history) {
      if (history == null) {
        throw new IllegalArgumentException("Argument \"history\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("history", history);
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    @NonNull
    public Bundle getArguments() {
      Bundle __result = new Bundle();
      if (arguments.containsKey("history")) {
        String history = (String) arguments.get("history");
        __result.putString("history", history);
      }
      return __result;
    }

    @Override
    public int getActionId() {
      return R.id.actionHistoryFragment;
    }

    @SuppressWarnings("unchecked")
    @NonNull
    public String getHistory() {
      return (String) arguments.get("history");
    }

    @Override
    public boolean equals(Object object) {
      if (this == object) {
          return true;
      }
      if (object == null || getClass() != object.getClass()) {
          return false;
      }
      ActionHistoryFragment that = (ActionHistoryFragment) object;
      if (arguments.containsKey("history") != that.arguments.containsKey("history")) {
        return false;
      }
      if (getHistory() != null ? !getHistory().equals(that.getHistory()) : that.getHistory() != null) {
        return false;
      }
      if (getActionId() != that.getActionId()) {
        return false;
      }
      return true;
    }

    @Override
    public int hashCode() {
      int result = 1;
      result = 31 * result + (getHistory() != null ? getHistory().hashCode() : 0);
      result = 31 * result + getActionId();
      return result;
    }

    @Override
    public String toString() {
      return "ActionHistoryFragment(actionId=" + getActionId() + "){"
          + "history=" + getHistory()
          + "}";
    }
  }
}
