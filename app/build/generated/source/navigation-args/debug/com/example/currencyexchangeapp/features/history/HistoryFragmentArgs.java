package com.example.currencyexchangeapp.features.history;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.navigation.NavArgs;
import java.lang.IllegalArgumentException;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.HashMap;

public class HistoryFragmentArgs implements NavArgs {
  private final HashMap arguments = new HashMap();

  private HistoryFragmentArgs() {
  }

  private HistoryFragmentArgs(HashMap argumentsMap) {
    this.arguments.putAll(argumentsMap);
  }

  @NonNull
  @SuppressWarnings("unchecked")
  public static HistoryFragmentArgs fromBundle(@NonNull Bundle bundle) {
    HistoryFragmentArgs __result = new HistoryFragmentArgs();
    bundle.setClassLoader(HistoryFragmentArgs.class.getClassLoader());
    if (bundle.containsKey("history")) {
      String history;
      history = bundle.getString("history");
      if (history == null) {
        throw new IllegalArgumentException("Argument \"history\" is marked as non-null but was passed a null value.");
      }
      __result.arguments.put("history", history);
    } else {
      throw new IllegalArgumentException("Required argument \"history\" is missing and does not have an android:defaultValue");
    }
    return __result;
  }

  @SuppressWarnings("unchecked")
  @NonNull
  public String getHistory() {
    return (String) arguments.get("history");
  }

  @SuppressWarnings("unchecked")
  @NonNull
  public Bundle toBundle() {
    Bundle __result = new Bundle();
    if (arguments.containsKey("history")) {
      String history = (String) arguments.get("history");
      __result.putString("history", history);
    }
    return __result;
  }

  @Override
  public boolean equals(Object object) {
    if (this == object) {
        return true;
    }
    if (object == null || getClass() != object.getClass()) {
        return false;
    }
    HistoryFragmentArgs that = (HistoryFragmentArgs) object;
    if (arguments.containsKey("history") != that.arguments.containsKey("history")) {
      return false;
    }
    if (getHistory() != null ? !getHistory().equals(that.getHistory()) : that.getHistory() != null) {
      return false;
    }
    return true;
  }

  @Override
  public int hashCode() {
    int result = 1;
    result = 31 * result + (getHistory() != null ? getHistory().hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "HistoryFragmentArgs{"
        + "history=" + getHistory()
        + "}";
  }

  public static class Builder {
    private final HashMap arguments = new HashMap();

    public Builder(HistoryFragmentArgs original) {
      this.arguments.putAll(original.arguments);
    }

    public Builder(@NonNull String history) {
      if (history == null) {
        throw new IllegalArgumentException("Argument \"history\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("history", history);
    }

    @NonNull
    public HistoryFragmentArgs build() {
      HistoryFragmentArgs result = new HistoryFragmentArgs(arguments);
      return result;
    }

    @NonNull
    public Builder setHistory(@NonNull String history) {
      if (history == null) {
        throw new IllegalArgumentException("Argument \"history\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("history", history);
      return this;
    }

    @SuppressWarnings("unchecked")
    @NonNull
    public String getHistory() {
      return (String) arguments.get("history");
    }
  }
}
